# from pprint import pprint
# from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView

from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.db.models import Count

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    # login_url = "/login/"
    model = Project
    template_name = "projects/list.html"
    print("huh")

    def get_queryset(self):
        print(Project.tasks)
        # pprint(dir(Project.tasks.all))
        return Project.objects.filter(members=self.request.user)

    def get_context_data(self, **kwarg):
        context = super().get_context_data(**kwarg)
        context["taskcontext"] = Project.objects.annotate(
            number_of_tasks=Count("tasks")
        )
        return context


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/details.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "members", "description"]

    def form_valid(self, form):
        form.instance.account = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("show_project", kwargs={"pk": self.object.pk})
